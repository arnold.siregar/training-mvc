package com.wirecard.training.common;

import java.util.List;

public interface CrudDao<T> {
    List<T> findAll();

    T findById(long id);

    void add(T model);

    void update(long id, T model);

    void delete(long id);
}