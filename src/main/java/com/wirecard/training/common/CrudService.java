package com.wirecard.training.common;

import java.util.List;

public interface CrudService<T> {
    List<T> findAll();

    T findById(long id);

    void add(T model);
}