package com.wirecard.training.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hobby")
public class Hobby implements Serializable {
    private static final long serialVersionUID = -8289959932709364189L;

    @Id
    private long hid;
    @Column
    private String name;

    // @ManyToMany(mappedBy = "hobbies")
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "person_hobby", joinColumns = { @JoinColumn(name = "hid") }, inverseJoinColumns = {
            @JoinColumn(name = "pid") })
    private Set<Person> people;

    public long getHid() {
        return hid;
    }

    public void setHid(long id) {
        this.hid = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // public Set<Person> getPeople() {
    // return people;
    // }

    // public void setPeople(Set<Person> people) {
    // this.people = people;
    // }

    public Hobby() {
    }

    public Hobby(long id, String name) {
        this.hid = id;
        this.name = name;
        // this.people = new HashSet<Person>();
    }

    @Override
    public String toString() {
        return "Hobby [id=" + hid + ", name=" + name + "]";
    }

}