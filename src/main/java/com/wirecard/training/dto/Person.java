package com.wirecard.training.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person implements Serializable {

	private static final long serialVersionUID = 4181247624151664885L;

	@Id
	private long pid;
	@Column
	private String firstName;
	@Column
	private String lastName;
	@Column
	private Date dob;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "personId")
	private List<Vehicle> vehicles;

	@ManyToMany(mappedBy = "people")
	private Set<Hobby> hobbies;

	public Person() {
		super();
	}

	public Person(long id, String firstName, String lastName, Date dob) {
		super();
		this.pid = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dob = dob;
		hobbies = new HashSet<Hobby>();
	}

	public long getPid() {
		return pid;
	}

	public void setPid(long id) {
		this.pid = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public List<Vehicle> getVehicles() {
		return this.vehicles;
	}

	public Set<Hobby> getHobbies() {
		return this.hobbies;
	}

	public void setHobbies(Set<Hobby> hobbies) {
		this.hobbies = hobbies;
	}

	@Override
	public String toString() {
		return "Person [dob=" + dob + ", firstName=" + firstName + ", id=" + pid + ", lastName=" + lastName
				+ ", vehicles=" + vehicles + "]";
	}
}
