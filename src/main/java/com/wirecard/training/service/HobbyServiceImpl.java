package com.wirecard.training.service;

import java.util.List;

import com.wirecard.training.dao.HobbyDao;
import com.wirecard.training.dto.Hobby;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HobbyServiceImpl implements HobbyService {

    @Autowired
    private HobbyDao hobbyDao;

    @Override
    public List<Hobby> findAll() {
        return null;
    }

    @Override
    public Hobby findById(long id) {
        return null;
    }

    @Override
    public void add(Hobby model) {

    }

}