package com.wirecard.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.training.dao.VehicleDao;
import com.wirecard.training.dto.Vehicle;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleDao vehicleDao;

	@Override
	public List<Vehicle> findAll() {
		return vehicleDao.findAll();
	}

	@Override
	public Vehicle findById(long id) {
		return null;
	}

	@Override
	public void add(Vehicle model) {

	}
}
