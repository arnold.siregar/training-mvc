package com.wirecard.training.service;

import com.wirecard.training.common.CrudService;
import com.wirecard.training.dto.Vehicle;

public interface VehicleService extends CrudService<Vehicle> {
}
