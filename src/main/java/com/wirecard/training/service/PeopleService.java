package com.wirecard.training.service;

import java.util.List;

import com.wirecard.training.common.CrudService;
import com.wirecard.training.dto.Person;

public interface PeopleService extends CrudService<Person> {
    List<Person> findPersonVehicleAll();

    List<Person> findPersonHobbyAll();

    void addPersonWithHobby(Person person);
}