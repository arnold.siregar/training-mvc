package com.wirecard.training.service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wirecard.training.dao.HobbyDao;
import com.wirecard.training.dao.PersonDao;
import com.wirecard.training.dto.Hobby;
import com.wirecard.training.dto.Person;

@Service
public class PeopleServiceImpl implements PeopleService {

	@Autowired
	private PersonDao personDao;

	@Autowired
	private HobbyDao hobbyDao;

	@Override
	public List<Person> findAll() {
		List<Person> all = personDao.findAll();
		System.out.println("People: " + all);
		return all;
	}

	@Override
	public Person findById(long id) {
		return personDao.findById(id);
	}

	@Override
	public void add(Person model) {
		personDao.add(model);
	}

	@Override
	public List<Person> findPersonVehicleAll() {
		return personDao.findPersonVehicleAll();
	}

	@Override
	public List<Person> findPersonHobbyAll() {
		return personDao.findPersonHobbyAll();
	}

	@Override
	public void addPersonWithHobby(Person person) {
		int newId = personDao.findAll().size() + 1;

		List<Hobby> hobbies = hobbyDao.findAll();

		person = new Person(newId, "First-" + newId, "Last-" + newId, new Date(2001, 04, 01));

		Set<Hobby> personHobbies = new HashSet<Hobby>();
		personHobbies.add(hobbies.get(0));
		personHobbies.add(hobbies.get(3));
		person.setHobbies(personHobbies);

		personDao.addPersonWithHobby(person);
	}

	/*
	 * private List<Person> getFakePeople() { List<Person> people = new
	 * ArrayList<Person>();
	 * 
	 * Person p1 = new Person(1, "Adi", "Guna", new Date(1990, 06, 20)); Person p2 =
	 * new Person(2, "Budi", "Karya", new Date(2001, 03, 20)); Person p3 = new
	 * Person(3, "Caca", "Handika", new Date(1998, 05, 20));
	 * 
	 * people.add(p1); people.add(p2); people.add(p3);
	 * 
	 * return people; }
	 */
}
