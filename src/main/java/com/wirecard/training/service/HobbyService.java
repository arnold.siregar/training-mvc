package com.wirecard.training.service;

import com.wirecard.training.common.CrudService;
import com.wirecard.training.dto.Hobby;

public interface HobbyService extends CrudService<Hobby> {
}