package com.wirecard.training.dao;

import com.wirecard.training.common.CrudDao;
import com.wirecard.training.dto.Vehicle;

public interface VehicleDao extends CrudDao<Vehicle> {
}
