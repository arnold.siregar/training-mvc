package com.wirecard.training.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.wirecard.training.common.AbstractDao;
import com.wirecard.training.dto.Vehicle;

@Repository
public class VehicleDaoImpl extends AbstractDao implements VehicleDao {

	@Override
	public List<Vehicle> findAll() {
		List<Vehicle> list = null;
		Query query = null;
		String sql = "select v from Vehicle v";

		try {
			list = new ArrayList<Vehicle>();
			getSession().beginTransaction();
			query = getSession().createQuery(sql);
			list = query.list();

		} catch (Exception e) {
			System.out.println("VehicleDaoImpl error: " + e.getMessage());
		} finally {
			getSession().close();
		}

		System.out.println(list);
		return list;
	}

	@Override
	public Vehicle findById(long id) {
		return null;
	}

	@Override
	public void add(Vehicle model) {

	}

	@Override
	public void update(long id, Vehicle model) {

	}

	@Override
	public void delete(long id) {

	}
}
