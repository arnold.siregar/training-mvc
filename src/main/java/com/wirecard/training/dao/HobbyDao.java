package com.wirecard.training.dao;

import com.wirecard.training.common.CrudDao;
import com.wirecard.training.dto.Hobby;

public interface HobbyDao extends CrudDao<Hobby> {
}