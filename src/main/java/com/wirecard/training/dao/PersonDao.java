package com.wirecard.training.dao;

import java.util.List;

import com.wirecard.training.common.CrudDao;
import com.wirecard.training.dto.Person;

public interface PersonDao extends CrudDao<Person> {
    List<Person> findPersonVehicleAll();

    List<Person> findPersonHobbyAll();

    void addPersonWithHobby(Person person);
}
