package com.wirecard.training.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import com.wirecard.training.common.AbstractDao;
import com.wirecard.training.dto.Hobby;
import com.wirecard.training.dto.Person;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class PersonDaoImpl extends AbstractDao implements PersonDao {

	// Sql Queries
	private static final String SQL_PERSON_FIND_ALL = "from Person p";
	private static final String SQL_PERSON_FIND_BY_ID = "from Person p where p.id = :id";
	// one to many
	private static final String SQL_PERSON_VEHICLE_FIND_ALL = "from Person p inner join Vehicle v on p.id = v.personId";
	// many to many
	private static final String SQL_PERSON_HOBBY_FIND_ALL = "select distinct p from Person p join fetch p.hobbies h";

	@Override
	public List<Person> findAll() {
		List<Person> list = null;

		try {
			list = new ArrayList<Person>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(SQL_PERSON_FIND_ALL);
			list = query.list();
		} catch (Exception e) {
			System.out.println("PersonDaoImpl.findAll() error: " + e.getMessage());
		} finally {
			getSession().close();
		}

		return list;
	}

	@Override
	public Person findById(long id) {
		Person person = null;

		try {
			getSession().beginTransaction();
			Query query = getSession().createQuery(SQL_PERSON_FIND_BY_ID);
			query.setParameter("id", id);
			person = (Person) query.uniqueResult();
		} catch (Exception e) {
			System.out.println("PersonDaoImpl.findById() error: " + e.getMessage());
		} finally {
			getSession().close();
		}

		return person;
	}

	@Override
	public void add(Person model) {
		try {
			getSession().beginTransaction();
			getSession().persist(model);
			getSession().getTransaction().commit();
		} catch (Exception e) {
			System.out.println("PersonDaoImpl.add() error: " + e.getMessage());
		} finally {
			getSession().close();
		}
	}

	@Override
	public void update(long id, Person model) {
	}

	@Override
	public void delete(long id) {
	}

	@Override
	public List<Person> findPersonVehicleAll() {
		List<Person> list = null;

		try {
			list = new ArrayList<Person>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(SQL_PERSON_VEHICLE_FIND_ALL);
			list = query.list();

			System.out.println("findPersonVehicleAll:\n" + list);
		} catch (Exception e) {
			System.out.println("PersonDaoImpl.findPersonVehicleAll() error: " + e.getMessage());
		} finally {
			getSession().close();
		}

		return list;
	}

	@Override
	public List<Person> findPersonHobbyAll() {
		List<Person> list = null;

		try {
			list = new ArrayList<Person>();
			getSession().beginTransaction();
			Query query = getSession().createQuery(SQL_PERSON_HOBBY_FIND_ALL);
			list = query.list();

			System.out.println("findPersonHobbyAll:\n" + list);
		} catch (Exception e) {
			System.out.println("PersonDaoImpl.findPersonHobbyAll() error: " + e.getMessage());
		} finally {
			getSession().close();
		}

		for (Object ph : list.stream().toArray()) {
			// System.out.println("Name: " + ph.getFirstName() + " " + ph.getLastName() + ",
			// hobbies are:");
			System.out.println(ph.getClass().getTypeName());
		}

		return list;
	}

	@Override
	public void addPersonWithHobby(Person person) {
		try {
			getSession().beginTransaction();
			getSession().update(person);
			getSession().getTransaction().commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error on addPersonWithHobby(): " + e.getMessage());
		} finally {
			getSession().close();
		}
	}
}
