package com.wirecard.training.dao;

import java.util.ArrayList;
import java.util.List;

import com.wirecard.training.common.AbstractDao;
import com.wirecard.training.dto.Hobby;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class HobbyDaoImpl extends AbstractDao implements HobbyDao {

    private static final String SQL_HOBBY_FIND_ALL = "from Hobby h";

    @Override
    public List<Hobby> findAll() {
        List<Hobby> list = null;

        try {
            list = new ArrayList<Hobby>();
            getSession().beginTransaction();
            Query query = getSession().createQuery(SQL_HOBBY_FIND_ALL);
            list = query.list();
        } catch (Exception e) {
            System.out.println("HobbyDaoImpl.findAll() error: " + e.getMessage());
        } finally {
            getSession().close();
        }

        return list;
    }

    @Override
    public Hobby findById(long id) {
        return null;
    }

    @Override
    public void add(Hobby model) {

    }

    @Override
    public void update(long id, Hobby model) {

    }

    @Override
    public void delete(long id) {

    }

}