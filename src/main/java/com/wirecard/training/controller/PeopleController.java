package com.wirecard.training.controller;

import java.util.List;

import javax.validation.Valid;

import com.wirecard.training.dto.Person;
import com.wirecard.training.service.PeopleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/people")
public class PeopleController {

	@Autowired
	private PeopleService peopleService;

	@GetMapping
	public String people(Model model) {
		List<Person> personList = peopleService.findAll();
		model.addAttribute("people", personList);
		return "people";
	}

	@GetMapping("/add-person")
	public String addPerson(Model model) {
		return "add-person";
	}

	@PostMapping("/add-person")
	public String personAdded(@Valid @ModelAttribute("person") Person person, BindingResult result, Model model) {
		System.out.println("1 --" + person.toString());
		peopleService.add(person);
		return "redirect:/people";
	}

	@GetMapping("/{id}")
	public String viewPerson(@PathVariable long id, Model model) {
		Person person = peopleService.findById(id);
		model.addAttribute("person", person);
		return "view-person";
	}

	@GetMapping("/person-vehicle")
	public String findPersonVehicle(Model model) {
		List<Person> list = peopleService.findPersonVehicleAll();
		model.addAttribute("personVehicle", list);

		// test using generic List interface
		List list2 = peopleService.findPersonVehicleAll();
		for (Object obj : list2) {
			System.out.println(obj);
		}

		return "person-vehicle";
	}

	@GetMapping("/person-hobby")
	public String findPersonHobby(Model model) {
		List<Person> list = peopleService.findPersonHobbyAll();
		model.addAttribute("personHobby", list);

		for (Object obj : list) {
			System.out.println(obj);
		}

		return "person-hobby";
	}

	@PostMapping("/add-person-with-hobby")
	public String addPersonWithHobby() {
		peopleService.addPersonWithHobby(null);
		return "redirect:/people";
	}
}
