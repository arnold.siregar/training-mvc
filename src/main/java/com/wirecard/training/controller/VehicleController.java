package com.wirecard.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wirecard.training.dto.Vehicle;
import com.wirecard.training.service.VehicleService;

@Controller
@RequestMapping("/vehicles")
public class VehicleController {

	@Autowired
	private VehicleService vehicleService;

	@GetMapping
	public String findAll(Model model) {
		System.out.println("Vehicles.findAll()");

		List<Vehicle> vehicles = vehicleService.findAll();

		System.out.println(vehicles);
		model.addAttribute("vehicles", vehicles);
		return "vehicles";
	}

}
