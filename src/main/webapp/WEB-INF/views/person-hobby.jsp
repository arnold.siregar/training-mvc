<%@page import="org.springframework.ui.Model"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="ISO-8859-1">
    <link href="<c:url value='/resources/theme2/css/main.css' />" rel="stylesheet">
    <title>List of People</title>
</head>

<body>
    <ul>
        <c:forEach items="${personHobby}" var="ph">
            <hr />
            <div>
                <c:out value="${ph.firstName}" />
            </div>
            <li>
                <c:forEach items="${ph.hobbies}" var="h">
                    <div>
                        <c:out value="${h.name}" />
                    </div>
                </c:forEach>
            </li>
        </c:forEach>
    </ul>
</body>

</html>