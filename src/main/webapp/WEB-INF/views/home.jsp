<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>Hello MVC</title>
</head>

<body>
    <h2>Hello MVC World from WEB-INF</h2>
    <br />
    <h3>${message}</h3>
    <a href="home2">Goto Page 2</a> <br />
    <a href="people">People</a> <br />
    <a href="people/person-vehicle">Person - Vehicle</a> <br />
    <a href="people/person-hobby">Person - Hobby</a> <br />
    <a href="people/add-person">Add new Person</a> <br />
    <a href="vehicles">Vehicles</a> <br />
    <a href="vehicles/add-vehicle">Add new Vehicle</a> <br />

</body>

</html>