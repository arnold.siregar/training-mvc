<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<link href="<c:url value='/resources/theme2/css/main.css' />" rel="stylesheet">
	<title>Add New Person</title>
</head>

<body>
	<h3>Add new Person:</h3>
	<form name="frmPerson" target="/people/add-person" modelAttribute="person" method="POST">
		<label>Id</label>
		<input type="text" name="pid" />
		<br />
		<label>First Name</label>
		<input type="text" name="firstName" />
		<br />
		<label>Last Name</label>
		<input type="text" name="lastName" />
		<br />
		<label>Date of Birth</label>
		<input type="text" name="dob" />

		<button type="submit">Submit</button>
	</form>
</body>

</html>