<%@page import="org.springframework.ui.Model"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<title>List of Vehicles</title>
</head>

<body>
	<h3>Vehicles:</h3>
	<ul>
		<c:forEach items="${vehicles}" var="vehicle">
			<li>
				<c:out value="${vehicle.id}" />
				<c:out value="${vehicle.make}" />
				<c:out value="${vehicle.model}" />
				<c:out value="${vehicle.seat}" />
			</li>
		</c:forEach>
	</ul>
</body>

</html>