<%@page import="org.springframework.ui.Model"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="ISO-8859-1">
	<link href="<c:url value='/resources/theme2/css/main.css' />" rel="stylesheet">
	<title>List of People</title>
</head>

<body>
	<h3>People:</h3>
	<div>
		<a href="people/add-person">Add New Person</a>
	</div>

	<table class="table">
		<thead>
			<tr>
				<td>Id</td>
				<td>First Name</td>
				<td>Last Name</td>
				<td>Dob</td>
				<td>Vehicles</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${people}" var="person">
				<tr>
					<td>
						<a href="people/${person.hid}">
							<c:out value="${person.hid}" />
						</a>
					</td>
					<td>
						<c:out value="${person.firstName}" />
					</td>
					<td>
						<c:out value="${person.lastName}" />
					</td>
					<td>
						<c:out value="${person.dob}" />
					</td>
					<td>
						<ul>
							<c:forEach items="${person.vehicles}" var="vehicle">
								<li>
									<c:out value="${vehicle.make}" />
									<c:out value="${vehicle.model}" />
								</li>
							</c:forEach>
						</ul>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>

</html>