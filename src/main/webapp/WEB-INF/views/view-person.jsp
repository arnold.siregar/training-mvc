<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="ISO-8859-1">
    <title>View Person</title>
</head>

<body>
    <label>Id :</label> ${person.hid}
    <label>First Name:</label> ${person.firstName}
    <label>Last Name:</label> ${person.lastName}
    <label>DOB:</label> ${person.dob}

    <!-- <br />
    <label>Vehicles:</label>
    <ul>
        <c:foreach items="${person.vehicles}" var="vehicle">
            <li>
                <c:out value="${vehicle.make}" />
                <c:out value="${vehicle.model}" />
            </li>
        </c:foreach>
    </ul>
    <br />
    <button>Add Vehicle</button> -->
</body>

</html>